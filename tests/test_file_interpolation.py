from bgconfig import BGConfig


def test_path_interpolation():
    config = BGConfig(
        './data/test1.conf.template',
        'test1',
        config_file='./data/test1.conf',
        config_spec='./data/test1.conf.spec',
        use_bgdata=False
    )

    print("test: ", config['test'])
    assert config['test'] == "value from file"

    print("bgdata: ", config['bgdata'])
    assert config['bgdata'] == "%(bgdata://datasets/genedetails/1.0?latest)"

    print("var", config['var'])
    assert config['var'] == "${myvar}"


def test_path_and_bgdata_interpolation():
    config = BGConfig(
        './data/test1.conf.template',
        'test1',
        config_file='./data/test1.conf',
        config_spec='./data/test1.conf.spec'
    )

    print("test: ", config['test'])
    assert config['test'] == "value from file"

    print("bgdata: ", config['bgdata'])
    assert "genedetails.json.gz" in config['bgdata']

    print("var", config['var'])
    assert config['var'] == "${myvar}"


if __name__ == "__main__":
    test_path_interpolation()
    test_path_and_bgdata_interpolation()
